import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export interface Todo {
  id: number;
  task: string;
  completed: boolean;
}

export default new Vuex.Store<{ todos: Todo[] }>({
  state: {
    todos: [
      {
        id: 0,
        completed: true,
        task: "Learn Vuejs"
      },
      {
        id: 1,
        completed: false,
        task: "Kick ass"
      }
    ]
  },
  mutations: {
    addTodo: (state, task: string) =>
      (state.todos = [
        ...state.todos,
        { task, id: Math.random(), completed: false }
      ]),
    deleteTodo: (state, id: number) =>
      (state.todos = state.todos.filter(t => t.id !== id)),
    toggleTodo: (state, id: number) =>
      (state.todos = state.todos.map(
        t => (t.id === id ? { ...t, completed: !t.completed } : t)
      ))
  },
  actions: {}
});
